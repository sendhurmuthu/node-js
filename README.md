# README #

### What is this repository for? ###

* Node js
* MySQL

### How to set up? ###

* Run index.js file to create a database
* Then run db-createtable in cmd to create table in the database
* For inserting values into the table use db-insertinto.js file
* Deletion of record can be done using db-deleteRecord.js file
* Updation of table is done using db-updateTable.js file


### PS: ###
* I did'nt know how to use Heroku
* Uploaded this repo in BitBucket instead of GitHub